import { put, all, takeLatest, call } from "redux-saga/effects";

import { types as typesPosts } from "../redux/actions/posts.action";

import { setPosts } from "../redux/actions/posts.action";
import { getSingles, getSinglesWithTag } from "../requests/posts";

export function* getPostsSaga() {
  const { data } = yield call(getSingles);

  yield put(setPosts(data));
}

export function* getPostsWithQuerySaga(action) {
  if (action.payload.length > 4) {
    const { data } = yield call(getSinglesWithTag, action.payload);

    yield put(setPosts(data));
  }

  if (action.payload.length === 0) {
    yield getPostsSaga();
  }
}

export function* watchPostsSagas() {
  yield all([takeLatest(typesPosts.GET_POSTS, getPostsSaga)]);
  yield all([takeLatest(typesPosts.SET_QUERY, getPostsWithQuerySaga)]);
}
