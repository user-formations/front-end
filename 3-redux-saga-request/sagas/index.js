import { all, fork } from "redux-saga/effects";

import { watchPostsSagas } from "./posts.saga";

function* rootSaga() {
  yield all([fork(watchPostsSagas)]);
}

export default rootSaga;
