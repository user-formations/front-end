export const types = {
  GET_POSTS: "posts/GET_POSTS",
  SET_POSTS: "posts/SET_POSTS",
  SET_QUERY: "posts/SET_QUERY",
};

export function getPosts() {
  return {
    type: types.GET_POSTS,
  };
}

export function setPosts(payload) {
  return {
    type: types.SET_POSTS,
    payload,
  };
}

export function setQuery(payload) {
  return {
    type: types.SET_QUERY,
    payload,
  };
}
