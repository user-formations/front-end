import { types as typesPosts } from "../actions/posts.action";

const initialState = {
  allPosts: [],
  query: "",
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case typesPosts.SET_POSTS:
      let newAllPosts = action.payload;
      return {
        ...state,
        allPosts: newAllPosts,
      };
    case typesPosts.SET_QUERY:
      return {
        ...state,
        query: action.payload,
      };

    default:
      return state;
  }
}

export default reducer;
