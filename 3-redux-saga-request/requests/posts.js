const axios = require("axios");

export const getSingles = () => axios.get("https://dev.to/api/articles");
export const getSinglesWithTag = (tag) =>
  axios.get(`https://dev.to/api/articles?tag=${tag}`);
