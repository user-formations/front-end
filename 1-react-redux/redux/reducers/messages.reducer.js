import { types as typesMessages } from "../actions/messages.action";

const initialState = {
  allMessages: [],
  display: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case typesMessages.NEW:
      let newAllMessages = [...state.allMessages, action.payload];
      return {
        ...state,
        allMessages: newAllMessages,
      };
    default:
      return state;
  }
}

export default reducer;
