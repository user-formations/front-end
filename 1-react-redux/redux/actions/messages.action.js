export const types = {
  NEW: "messages/NEW",
};

export function newMessage(payload) {
  return {
    type: types.NEW,
    payload,
  };
}
