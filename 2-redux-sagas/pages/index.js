import React, { useState } from "react";
import { QuestionMarkCircleIcon } from "@heroicons/react/solid";
import { useSelector, useDispatch } from "react-redux";

import { newMessage } from "../redux/actions/messages.action";

// Sample datas
const user = {
  name: "Whitney Francis",
  email: "whitney@example.com",
  imageUrl:
    "https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=256&h=256&q=80",
};

export default function Home() {
  // Mon hook pour le textarea
  const [message, setMessage] = useState("");
  const dispatch = useDispatch();

  // Mon hooks pour récupérer les messages dans le store Redux
  const messages = useSelector((state) => state.messages.allMessages);

  const addMessage = (e) => {
    e.preventDefault();

    dispatch(
      newMessage({
        name: "Leslie Alexander",
        date: new Date(Date.now()).toLocaleDateString("fr-FR", {
          weekday: "long",
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "2-digit",
          minute: "2-digit",
        }),
        imageId: "1494790108377-be9c29b29330",
        body: message,
      })
    );

    setMessage("");
  };

  return (
    <div className="space-y-6 lg:col-start-1 lg:col-span-2 container mx-auto mt-10">
      <section aria-labelledby="notes-title">
        <div className="bg-white shadow sm:rounded-lg sm:overflow-hidden">
          <div className="divide-y divide-gray-200">
            <div className="px-4 py-5 sm:px-6">
              <h2
                id="notes-title"
                className="text-lg font-medium text-gray-900"
              >
                Messages
              </h2>
            </div>
            <div className="px-4 py-6 sm:px-6">
              <ul className="space-y-8">
                {messages.map((message, key) => (
                  <li key={key}>
                    <div className="flex space-x-3">
                      <div className="flex-shrink-0">
                        <img
                          className="h-10 w-10 rounded-full"
                          src={`https://images.unsplash.com/photo-${message.imageId}?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80`}
                          alt=""
                        />
                      </div>
                      <div>
                        <div className="text-sm">
                          <a href="#" className="font-medium text-gray-900">
                            {message.name}
                          </a>
                        </div>
                        <div className="mt-1 text-sm text-gray-700">
                          <p>{message.body}</p>
                        </div>
                        <div className="mt-2 text-sm space-x-2">
                          <span className="text-gray-500 font-medium">
                            {message.date}
                          </span>{" "}
                          <span className="text-gray-500 font-medium">
                            &middot;
                          </span>{" "}
                          <button
                            type="button"
                            className="text-gray-900 font-medium"
                          >
                            Reply
                          </button>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-6 sm:px-6">
            <div className="flex space-x-3">
              <div className="flex-shrink-0">
                <img
                  className="h-10 w-10 rounded-full"
                  src={user.imageUrl}
                  alt=""
                />
              </div>
              <div className="min-w-0 flex-1">
                <form onSubmit={addMessage}>
                  <div>
                    <label htmlFor="comment" className="sr-only">
                      About
                    </label>
                    <textarea
                      id="comment"
                      name="comment"
                      rows={3}
                      className="shadow-sm block w-full focus:ring-blue-500 focus:border-blue-500 sm:text-sm border border-gray-300 rounded-md py-2 px-2"
                      placeholder="Add a note"
                      value={message}
                      onChange={(e) => setMessage(e.target.value)}
                    />
                  </div>
                  <div className="mt-3 flex items-center justify-between">
                    <a
                      href="#"
                      className="group inline-flex items-start text-sm space-x-2 text-gray-500 hover:text-gray-900"
                    >
                      <QuestionMarkCircleIcon
                        className="flex-shrink-0 h-5 w-5 text-gray-400 group-hover:text-gray-500"
                        aria-hidden="true"
                      />
                      <span>Some HTML is okay.</span>
                    </a>
                    <button
                      type="submit"
                      className="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                      onClick={addMessage}
                    >
                      Comment
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
