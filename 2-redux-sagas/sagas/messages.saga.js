import { put, all, takeLatest } from "redux-saga/effects";
import { types as typesMessages } from "../redux/actions/messages.action";

import { setMessage } from "../redux/actions/messages.action";

export function* newMessageSaga(action) {
  console.log("NEW from saga");

  yield put(setMessage(action.payload));
}

export function* watchMessagesSagas() {
  yield all([takeLatest(typesMessages.NEW, newMessageSaga)]);
}
