import { all, fork } from "redux-saga/effects";

import { watchMessagesSagas } from "./messages.saga";

function* rootSaga() {
  yield all([fork(watchMessagesSagas)]);
}

export default rootSaga;
