export const types = {
  NEW: "messages/NEW",
  SET: "messages/SET",
};

export function newMessage(payload) {
  return {
    type: types.NEW,
    payload,
  };
}

export function setMessage(payload) {
  return {
    type: types.SET,
    payload,
  };
}
